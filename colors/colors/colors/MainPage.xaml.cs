﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace colors
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

        void OnSliderValueChanged(object sender, ValueChangedEventArgs arg)
        {

            if (sender == id_sl_red)
             {
                String x = id_sl_red.Value.ToString();
                labelred.Text = x;
             }
            else if (sender == id_sl_green)
            {
                String x = id_sl_green.Value.ToString();
                labelgreen.Text = x;
            }
            else if (sender == id_sl_blue)
            {
                String x = id_sl_blue.Value.ToString();
                labelblue.Text = x;
            }
           else if (sender == id_sl_alpha)
            {
                String x = id_sl_alpha.Value.ToString();
                labelalpha.Text = x;
            }
           
            id_bv_background.BackgroundColor=Color.FromRgba((int)id_sl_red.Value, (int)id_sl_green.Value, (int)id_sl_blue.Value, (int)id_sl_alpha.Value);
            
        }

        async private void ValidateUser(object sender, EventArgs e)
        {
          
                await Navigation.PushAsync(new Page1());
         
        }





    }
}
