﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace colors
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page1 : ContentPage
	{
		public Page1 ()
		{
			InitializeComponent ();
		}
        void OnSliderValue(object sender, ValueChangedEventArgs arg)
        {

            if (sender == id_sl_alto)
            {
                String variable= id_sl_alto.Value.ToString();
                labelalto.Text = variable;
            }
            else if (sender == id_sl_ancho)
            {
                String variable = id_sl_ancho.Value.ToString();
                labelancho.Text = variable;
            }
            id_bv_background.HeightRequest = (int)id_sl_alto.Value;
            id_bv_background.WidthRequest = (int)id_sl_ancho.Value;
            


        }
    }
}